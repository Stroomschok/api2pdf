<?php

namespace Tobias;

class Table
{
	/**
	 * Generate html (title, search parameters block and table)
	 *
	 * @param array $films
	 * @param array $parameters
	 * 
	 * @return string
	 */ 
    public function generate($filteredFilms, $parameters)
    {
        $title = '<h1>Films</h1>';

        //prepare search parameters for search input paragraph
        if (isset($parameters['startYear'])) {
            $startYear = 'Start year: ' . $parameters['startYear'];
        }

        if (isset($parameters['endYear'])){
            $endYear = 'End year: ' . $parameters['endYear'];
        }

        if (isset($parameters['query'])){
            $query = 'Query: ' . $parameters['query'];
        }
            
        // $outputParameters = '
        // <p>' .
        // (isset($startYear) ? $startYear : 'No start year given') .
        // (isset($endYear) ? $endYear : 'No end year given') .
        // (isset($query) ? $query : 'No query given') . '<p>';


        //prepare rows for generating table from films array
        $rows = '';

        foreach ($filteredFilms as $film) {

            $row = '<tr>
                        <td>' . $film->getTitle() . '</td>
                        <td>' . $film->getYear() . '</td>
                        <td>' . implode(',', $film->getActors()) . '</td>   
                    </tr>';

            $rows .= $row;
        }

        //construct table
        $table = '      
            <table class="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Cast</th>
                    </tr>
                </thead>

                <tbody>' . 
                    $rows .
                '</tbody>
            </table>';

        //combine all previous components 
        $output = $title . $outputParameters . $table;

        return $output;
    }
}

