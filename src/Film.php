<?php

namespace Tobias;

class Film
{
	private $title;

	private $year;

	private $cast = [];


	/**
	 * Get the value of title
	 */ 
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set the value of title
	 *
	 * @return  self
	 */ 
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get the value of year
	 * 
	 * @return int
	 */ 
	public function getYear()
	{
		return (int) $this->year;
	}

	/**
	 * Set the value of year
	 *
	 * @param int $year
	 * 
	 * @return  self
	 */ 
	public function setYear($year)
	{
		$this->year = (int) $year;

		return $this;
	}

	/**
	 * Get the value of cast
	 * 
	 * @return array
	 */ 
	public function getActors()
	{
		return $this->actors;
	}

	/**
	 * Set the value of cast
	 *
	 * @param array $cast
	 * 
	 * @return self
	 */ 
	public function setActors(array $actors)
	{
		$this->actors = $actors;

		return $this;
	}
}