<?php
require 'vendor/autoload.php';


// // reference the Dompdf namespace
// use Dompdf\Dompdf;

// // instantiate and use the dompdf class
// $dompdf = new Dompdf();
// $dompdf->loadHtml('hello world');

// // (Optional) Setup the paper size and orientation
// $dompdf->setPaper('A4', 'landscape');

// // Render the HTML as PDF
// $dompdf->render();

// // Output the generated PDF to Browser
// $dompdf->stream();
// exit;
		
		
$cache = new Gilbitron\Util\SimpleCache();
$cache->cache_time = 86400;

$films = json_decode($cache->get_data('movies', 'https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json'), true);

$filteredFilms = [];

foreach ($films as $filmItem) {

	// if (isset($_POST['startYear']) || isset($_POST['endYear'])) {
		
	// 	if ( (int) $filmItem['year'] < (int) $_POST['startYear'] || 
	// 		(int) $filmItem['year'] > (int) $_POST['endYear']) {
			
	// 		continue;
	// 	}
	// }

	
// 	if (isset($_POST['query'])) {

// 		$foundCast = false;
// 		// Check if cast value of film matches query, set foundCast boolean
// 		if ($_POST['query'] !== "") {
// 			foreach ($filmItem['cast'] as $cast) {
// 				if (strpos($cast, $_POST['query']) !== false) {
// 					$foundCast = true;
// 				}
// 			}
// 		}

// 		// Add to result array if there is no query or query matches title or foundCast is true
// 		if ($_POST['query'] === "" || strpos($filmItem['title'], $_POST['query']) !== false || $foundCast === true) {
			
			$film = new Tobias\Film;
			
			$film->setTitle($filmItem['title']);
			$film->setYear($filmItem['year']);
			$film->setActors($filmItem['cast']);
			$filteredFilms[] = $film;
// 		}
// 	}
}

//var_dump($filteredFilms);
$table = new Tobias\Table;
$output = $table->generate($filteredFilms, []);

// if (sizeof($_POST) != 0) {
// 	$parameters = [$_POST['startYear'],$_POST['endYear'],$_POST['query']];
	
// 	$dompdf = new Dompdf\Dompdf();
// 	$dompdf->loadHtml($output);

// 	$output = "<html><head></head><body>" . $output . "</body></html>";

// 	$dompdf->render();
	
// }

$text = nl2br("- Start een nieuw PHP project, dus niet gebaseerd op Laravel of Antheap
	- Maak een API call naar https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json
	- De JSON moet 1 dag gecached worden.
	- Maak een 'Film' class in PHP, en laad alle JSON 'entries' in.
	- Maak een HTML formuliertje met een Begindatum, Einddatum, Trefwoord, en 'Maak PDF' button.
	- De PDF moet alleen films tonen uit die periode en waar een bepaald trefwoord in voorkomt (mits velden zijn ingevuld).
	- Maak gebruik van https://github.com/dompdf/dompdf voor het maken van de PDF.
	- Maak gebruik van docblocks en comments.");

echo $text;    

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>API 2 PDF</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <H1>API 2 PDF</H1>
      

 <hr>
	<form method="get" action="index.php">
		<div class="form-group">
		<label for="inputStartYear">Start Year:</label>
		<input type="number"
			class="form-control" name="startYear" id="inputStartYear">
		</div>

		<div class="form-group">
		<label for="inputEndYear">End Year:</label>
		<input type="number"
			class="form-control" name="endYear" id="inputEndYear">
		</div>

		<div class="form-group">
		<label for="inputQuery">Query:</label>
		<input type="input"
			class="form-control" name="query" id="inputQuery">
		</div>

		<button type="submit">Submit</button>
	</form>
    
<?php
	echo $output;
?>
</body>
</html>